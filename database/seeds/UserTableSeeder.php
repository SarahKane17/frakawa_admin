<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users');
        $users = [[
            'name'=> 'frakawa',
//            'second_name' => '_',
            'password' => Hash::make('12345678'),
            'email' => 'frakawa@gmail.com',
            'details' => 'My name is Francis K Waweru and I love Art. I have grown to be a visual artist ever since I can remember. I work hard 6 days a week to make myself better in this field that I love.

I was born I Transnzoi county Kenya where I grew up with the same passion. At the age of 19 I relocated to Nairobi where I joined Bobea Arts Centre school of fine arts. Here I studied hard due to my passion to learn and grow more in the world of art. I had the privilege to learn from Pascal Chuma ( one of the best visual artists in Kenya).

My artworks reflects the inner me and I am inspired by different things in life, and they are; Love for colour, facial characters of different people, landscapes which are God’s creation and much more. I strive to push myself to the limit and learn something new in every painting that I do.'
        ]
        ];
        DB::table('users')-> insert($users);
    }
}
