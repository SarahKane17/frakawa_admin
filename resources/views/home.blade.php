@extends('layouts.master')
@section('content')
<div class="parallax">
    <h2 style="color: #ffffff; text-align: center; line-height: 700px; margin-bottom: -100px;">Every artist was first an amateur</h2>
    <p style="color: #ffffff; text-align: center;margin-top: -300px;">Ralph Waldo Emerson</p>
    {{--            <p >Ralph Waldo Emerson</p>--}}
</div>

{{--<div class="container">--}}
{{--    <div id="myCarousel" class="carousel slide " data-ride="carousel">--}}
{{--        <!-- Indicators -->--}}
{{--        <ol class="carousel-indicators">--}}
{{--            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>--}}
{{--            <li data-target="#myCarousel" data-slide-to="1"></li>--}}
{{--            <li data-target="#myCarousel" data-slide-to="2"></li>--}}
{{--            <li data-target="#myCarousel" data-slide-to="3"></li>--}}
{{--        </ol>--}}

{{--        <!-- Wrapper for slides -->--}}
{{--        <div class="carousel-inner">--}}

{{--            <div class="carousel-item active">--}}
{{--                <img src="{{asset('images/brushes.jpg')}}" alt="Los Angeles" style="width:100%;">--}}
{{--                <div class="carousel-caption">--}}
{{--                    <h3>Every artist was first an amateur</h3>--}}
{{--                    <p>Ralph Waldo Emerson</p>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            <div class="carousel-item">--}}
{{--                <img src="{{asset('images/brush.jpg')}}" alt="Chicago" style="width:100%;">--}}
{{--                <div class="carousel-caption">--}}
{{--                    <h3>Creativity takes courage</h3>--}}
{{--                    <p>Henri Matisse</p>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            <div class="carousel-item">--}}
{{--                <img src="{{asset('images/bicycle.jpg')}}" alt="New York" style="width:100%;">--}}
{{--                <div class="carousel-caption">--}}
{{--                    <h3>We don’t make mistakes, just happy little accidents</h3>--}}
{{--                    <p>Bob Ross</p>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            <div class="carousel-item">--}}
{{--                <img src="{{asset('images/graffiti.jpg')}}" alt="New York" style="width:100%;">--}}
{{--                <div class="carousel-caption">--}}
{{--                    <h3>A picture is a poem without words</h3>--}}
{{--                    <p>Horace</p>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--        </div>--}}

{{--        <!-- Left and right controls -->--}}
{{--        <a class="left carousel-control-prev" href="#myCarousel" onclick="$('#myCarousel').carousel('prev')" data-slide="prev">--}}
{{--            <span class="carousel-control-prev-icon" aria-hidden="true"></span>--}}
{{--            <span class="sr-only">Previous</span>--}}
{{--        </a>--}}
{{--        <a class="right carousel-control-next" href="#myCarousel" onclick="$('#myCarousel').carousel('next')" data-slide="next">--}}
{{--            <span class="carousel-control-next-icon" aria-hidden="true"></span>--}}
{{--            <span class="sr-only">Next</span>--}}
{{--        </a>--}}
{{--    </div>--}}
{{--</div>--}}



{{--<section>--}}
{{--    <div class="container">--}}
{{--        <div class="row">--}}
{{--            don't know what's here yet--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</section>--}}

<div class="row">&nbsp;</div>
<div>&nbsp;</div>

<section class="content-row in-view">
    <div class="container">
        <div style="border: 5px solid #A9ACB1; padding: 50px;" class="row">
            <div class="col-lg-5">
                <img style="height: 400px; width: 400px;" class="imgdesc" src="{{asset('images/frakawa-blue.jpeg')}}">
            </div>

            <div class="col-lg-7">
                <h1 style="font-family: 'Titillium Web', sans-serif;font-weight: 600;">Hi there,</h1>
                <p style="font-family: 'Titillium Web', sans-serif;font-weight: 400; font-size: 17px; font-style: italic">My name is Francis K Waweru and I love Art. I have grown to be a visual artist ever since  I can remember. I work hard 6 days a week to make  myself better in this field  that I love.<br><br>
                    I was born I Transnzoi county Kenya where I grew up with the same passion. At the age of 19 I relocated to Nairobi  where I joined Bobea Arts Centre school of fine arts. Here I studied hard due to my passion to learn and grow more in the world of art. I had the privilege to learn from Pascal Chuma ( one of the best visual artists in Kenya).<br><br>
                    My artworks reflects the inner me and I am inspired by different things in life, and they are; Love for colour, facial characters of different people, landscapes which are God’s creation and much more. I strive to push myself to the limit and learn something new in every painting that I do.</p>
                {{--                        <span><a href="" style="color: blue;">Read more..</a></span>--}}
            </div>
        </div>
    </div>
</section>

<div>&nbsp;</div>

<div class="back-image-pic" style="background-color: #F6F6F6; width: 100%;">

    <h3 style="padding-top: 5%; font-family: 'Titillium Web', sans-serif;font-weight: 1200;text-align: center; font-style: bold;font-size: 40px">Some of my work</h3>


    <div>&nbsp;</div>

    <div class="spe-cor">
        <div class="container" style="margin-top: 5%;">
            <div class="row">
                <div id="slider-2" class="carousel carousel-by-item slide" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                        <div class="carousel-item active">
                            <div class="col-md-3 col-sm-4 col-12">
                                <img class="d-block img-fluid" style="height: 270px; width:350px" src="{{asset('images/gallery1.jpeg')}}" alt="First slide">
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-3 col-sm-4 col-12">
                                <img class="d-block img-fluid" style="height: 270px; width:350px;" src="{{asset('images/gallery2.jpeg')}}" alt="First slide">
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-3 col-sm-4 col-12">
                                <img class="d-block img-fluid" style="height: 270px; width:350px" src="{{asset('images/gallery3.jpeg')}}" alt="First slide">
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-3 col-sm-4 col-12">
                                <img class="d-block img-fluid" style="height: 270px; width:350px" src="{{asset('images/gallery4.jpeg')}}" alt="Second slide">
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-3 col-sm-4 col-12">
                                <img class="d-block img-fluid" style="height: 270px; width:350px" src="{{asset('images/gallery8.jpeg')}}" alt="Third slide">
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-3 col-sm-4 col-12">
                                <img class="d-block img-fluid" style="height: 270px; width:350px" src="{{asset('images/gallery6.jpeg')}}" alt="Third slide">
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-md-3 col-sm-4 col-12">
                                <img class="d-block img-fluid" style="height: 270px; width:350px" src="{{asset('images/gallery7.jpeg')}}" alt="Third slide">
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#slider-2" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#slider-2" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                <a class="btn btn-primary" style="text-align: center; margin-left: 41%; margin-top: 7%; background-color: #131f2e; border-color: #154596; border-radius: 0; width: 200px;" href="">See more</a>
            </div>
        </div>
    </div>
</div>



<div>&nbsp;</div>
<div>&nbsp;</div>

<section class="content-row in-view">
    <div class="container">
        <div style="border: 5px solid #A9ACB1; padding: 50px;" class="row">
            <div style="padding: 5%" class="col-lg-8">

                <h1 style="border-bottom: 2.5px solid ; font-family: 'Titillium Web', sans-serif;font-weight: 400;">No need to lie</h1>
                <i class="fa fa-quote-left" aria-hidden="true"></i>
                <p style="font-family: 'Titillium Web', sans-serif;font-weight: 400; font-style: italic">
                    This 97 by 70cm painting is titled:
                    " No need to lie" . It  was inspired by the
                    few moments with my uncle who is a smoker. If I did anything wrong as a child
                    and I was ready not to tell the truth, he would look me in the eyes and use the phrase "
                    No need to lie"
                </p>
                <i style="float: right" class="fa fa-quote-right" aria-hidden="true"></i>
            </div>
            <div class="col-lg-4">
                <img class="imgdesc" src="{{asset('images/gallery8.jpeg')}}">
            </div>
        </div>
    </div>
</section>


<div style="margin: 1%">
    &nbsp
</div>
@endsection



