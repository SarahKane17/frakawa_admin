<div class="footer" >
    <div class="back-image">
        <div class="flex-container" style="opacity: 1">
            <div style="color: #ffffff; margin-left: 13%; margin-top: 20px; width: 200px;">
                <h3 style="color: grey">Thank you for visiting my website!</h3>
            </div>
            <div style="margin-left: 17%;color: #ffffff;margin-top: 20px;">
                <div class="font2" style="width:150px;">Follow me on:<br></div>
                <br>
                <a style="font-style: italic;" class="font1" href="https://www.instagram.com/francis_k_waweru?r=nametag">
                    <i class="fa fa-instagram"> <i>instagram</i></i>
                </a>
                <br><br>
                <a style="font-style: italic;" class="font1" href="https://m.facebook.com/frakawa.arts?ref=bookmarks">
                    <i class="fa fa-facebook-official"> <i>facebook</i></i>
                </a>
                <br><br>
            </div>
            <div style="margin-left: 18%;margin-top: 20px; width: 200px;">
                <form action="{{route('messages.create')}}" method="POST" class="" role="form" autocomplete="off" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <textarea name="message" style="border-radius: 5px; border-color: transparent; height:70px;" placeholder="Write me sth"></textarea>
                    <input name="email" style="margin-top: 10px;border-radius: 5px;" placeholder="email"><br>
                    <button class="btn btn-primary" style="margin-top: 10px; background-color: #ff4500;border-radius: 5px; border-color: transparent; color: #ffffff">Send</button>
                </form>
            </div>
        </div>
    </div>
</div>

