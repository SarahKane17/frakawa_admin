<div class="row flex-container sticky primary-color">
    <div> <img class="logo-image" src="{{asset('images/frakawa-logo2.png')}}"></div>
    <div class="flex-container nav-list">
        <a class="nav-item" href="{{url('/')}}">Home</a>
        <a class="nav-item" href="{{url('/gallery')}}">Gallery</a>
        <a class="nav-item" href="">About</a>
        <a class="nav-item" href="">Contact</a>
    </div>
</div>
