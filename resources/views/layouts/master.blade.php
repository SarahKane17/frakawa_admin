<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/home.css') }}" >
{{--    <link rel="stylesheet" href="{{asset('js/custom.js') }}" >--}}
{{--    <link rel="stylesheet" href="{{asset('css/font-awesome.css')}}" />--}}
{{--    <link rel="stylesheet" href="{{asset('css/ionicons.css')}}" />--}}
{{--    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}" >--}}
    <link rel="stylesheet" href="{{asset('css/carousel-bootsnip.css')}}">
{{--    <link rel="stylesheet" href="{{asset('css/custom.css')}}">--}}

    {{--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>--}}
    {{--    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>--}}
    {{--    <script src="{{asset('js/bootstrap.js')}}"></script>--}}
    {{--    <script src="js/bootstrap.js"></script>--}}
    {{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Titillium+Web:ital,wght@0,200;0,300;0,400;0,600;1,200;1,300;1,400;1,600&display=swap" rel="stylesheet">
    <script src="{{asset('js/carousel-bootsnip.js')}}"></script>
    <style>


    </style>
    <title>Frakawa</title>
</head>
<body>
@include('layouts.navbar')
@yield('content')
@include('layouts.footer')
</body>
</html>

