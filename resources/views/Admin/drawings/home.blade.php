@extends('Admin.layouts.master')

@section('content')
    <div class="content">
        <div class="row right">
            <a href="{{route('drawings.new')}}" class="pull-right btn btn-primary" style="padding: 5px 50px 5px 50px; ">New</a>
        </div>
        <div class="row">&nbsp</div>

        <div class="card">
            <div class="card-header" style="margin-left: -1px;width: 100.2%">
                Drawings
            </div>
        </div>

        <div class="row">
            @foreach($drawings as $drawing)
                <div class="col-md-4">
                    <div class="thumbnail">
                        <img src="{{asset('storage/drawing/'.$drawing->image_url)}}" style="width:100%; height: 250px;">
                        <div class="caption">
                            <a href="{{route('drawings.destroy', $drawing->id)}}"><i class="fa fa-trash-o pull-right" aria-hidden="true"></i></a>
                        </div>
                        <div style=" margin-bottom: 5px">&nbsp;</div>
                    </div>
                </div>
            @endforeach

        </div>

    </div>
@endsection
