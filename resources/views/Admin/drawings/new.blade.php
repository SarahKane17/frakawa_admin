@extends('Admin.layouts.master')

@section('content')
    <div class="row center" >
        <div class="content col-lg-5">

            <div class="card">
                <div class="card-header">
                    <h5>New drawing</h5>
                </div>
                <div class="card-body">

                    <form action="{{route('drawings.create')}}" method="POST" class="" role="form" autocomplete="off" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label class="small-text">Drawing</label>
                            <input class="form-control input-lg form-control-sm" type="file" name="image_url" placeholder="image"
                                   value="" required="required">
                        </div>

{{--                        <div class="form-group">--}}
{{--                            <label class="small-text">Status</label>--}}
{{--                            <select name="status" class="custom-select">--}}
{{--                                <option value="1">None</option>--}}
{{--                                <option value="2">About me</option>--}}
{{--                                <option value="3">Favourite</option>--}}
{{--                            </select>--}}
{{--                        </div>--}}


                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection
