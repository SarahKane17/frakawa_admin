@extends('Admin.layouts.master')

@section('content')
    <div class="content">
{{--        <div class="row right">--}}
{{--            <a href="{{route('paintings.new')}}" class="pull-right btn btn-primary" style="padding: 5px 50px 5px 50px; ">New</a>--}}
{{--        </div>--}}
        <div class="row">&nbsp</div>

        <div class="card">
            @foreach($users as $user)
                <div class="card-header">
                    About me
                    <a class="pull-right" href="{{route('about_me.edit', $user->id)}}"><i class="fa fa-pencil-square-o center" aria-hidden="true"></i></a>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <img src="{{asset('storage/painting/'.$painting->image_url)}}" style="width:100%; height: 250px;">
                    </div>

                    <div class="col-lg-8">
                        <p>{{$user -> details}}</p>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
