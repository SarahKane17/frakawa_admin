@extends('Admin.layouts.master')

@section('content')
    <div class="row center" >
        <div class="content col-lg-5">

            <div class="card">
                <div class="card-header">
                    <h5>Edit about me</h5>
                </div>
                <div class="card-body">

                    <form action="{{route('about_me.update', $user->id)}}" method="POST" class="" role="form" autocomplete="off" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label class="small-text">About me</label>
                            <textarea name="about_me" rows="4" cols="50">{{$user->details}}</textarea>
                        </div>


                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection
