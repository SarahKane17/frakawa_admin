@extends('Admin.layouts.master')

@section('content')
    <div class="content">
        <div class="row right">
            <a href="{{route('paintings.new')}}" class="pull-right btn btn-primary" style="padding: 5px 50px 5px 50px; ">New</a>
        </div>
        <div class="row">&nbsp</div>

        <div class="card">
            <div class="card-header" style="margin-left: -1px;width: 100.2%">
                Paintings
            </div>
        </div>

        <div class="row">
            @foreach($paintings as $painting)
            <div class="col-md-4">
                <div class="thumbnail">
                    <img src="{{asset('storage/painting/'.$painting->image_url)}}" style="width:100%; height: 250px;">
                    <div class="caption">
                        <p style="text-align: center;">
                        <a href="{{route('paintings.edit', $painting->id)}}"><i class="fa fa-pencil-square-o center" aria-hidden="true"></i></a>
                        <a href="{{route('paintings.destroy', $painting->id)}}"><i class="fa fa-trash-o " aria-hidden="true"></i></a>
                        </p>
                    </div>
                    <div style=" margin-bottom: 5px">&nbsp;</div>
                </div>
            </div>
            @endforeach

        </div>

    </div>
@endsection
