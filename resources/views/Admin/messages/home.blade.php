@extends('Admin.layouts.master')

@section('content')
    <div class="content">
        @if(count($messages) > 0)
            @foreach($messages as $message)

                <div class="card">
                    <div class="card-header">
                        {{$message->email}}
                        <a class="pull-right" href="{{route('messages.destroy', $message->id)}}"><i class="fa fa-trash-o " aria-hidden="true"></i></a>
                    </div>
                    <div class="card-body">
                        {{$message->message}}
                    </div>
                </div>

            @endforeach
        @else
            <h4>No new messages yet</h4>
        @endif
    </div>
@endsection
