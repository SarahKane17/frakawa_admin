<div class="flex-container main-nav">
    <div class="main-nav-item flex-container">
        <a href="" class="tool-tip"><i class="fa fa-home"></i><span class="tooltip-text">Home</span></a>
{{--        <a href="" class="tool-tip"><i class="fa fa-cog"></i><span class="tooltip-text">Settings</span></a>--}}
{{--        <a href="" class="tool-tip"><i class="fa fa-user"></i><span class="tooltip-text">User</span></a>--}}
        <a href="{{ url('/admin/logout') }}" class="tool-tip"
           onclick="event.preventDefault();
           document.getElementById('logout-form').submit();">
            <i class="fa fa-sign-out"></i><span class="tooltip-text">Logout</span>
        </a>

        <form id="logout-form" action="{{ url('/admin/logout') }}" method="POST"
              style="display: none;">
            {{ csrf_field() }}
        </form>
    </div>
</div>

