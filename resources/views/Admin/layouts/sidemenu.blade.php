<div class="sidenav">
    <div class="flex-container  logo-background">
        <p style="align-content: center"><img class="logo" src="{{asset('images/frakawa-logo2.png')}}"></p>
    </div>

    <a class="{{ Request::path() == 'dashboard' ? 'active-nav' : '' }}" href="{{route('about_me')}}"><i class="fa fa-home p-1"></i> About me</a>
    <a class="{{ Request::path() == 'clients' ? 'active-nav' : '' }}" href="{{route('paintings')}}"><i class="fa fa-user p-1"></i> Paintings</a>
    <a class="{{ Request::path() == 'loan' ? 'active-nav' : '' }}" href="{{route('drawings')}}"><i class="fa fa-line-chart p-1"></i> Drawings</a>
    <a class="{{ Request::path() == 'payments' ? 'active-nav' : '' }}" href="{{route('messages')}}"><i class="fa fa-money p-1"></i> Messages</a>

    {{-- toggle menus   --}}
{{--    <div data-toggle="collapse" data-target="#finance" class="collapsed">--}}
{{--        <a class="" href="#"><i class="fa fa-caret-down"></i> Finance</a>--}}
{{--    </div>--}}
{{--    <div class="sub-menu collapse" id="finance">--}}
{{--        <a href=""><i class="fa fa-money p-1"></i> Expenses</a>--}}
{{--        <a href=""><i class="fa fa-money p-1"></i> Capital</a>--}}
{{--        <a href=""><i class="fa fa-money p-1"></i> Refunds</a>--}}

{{--    </div>--}}

{{--    <div data-toggle="collapse" data-target="#settings" class="collapsed">--}}
{{--        <a class="" href="#"><i class="fa fa-caret-down"></i> Settings</a>--}}
{{--    </div>--}}
{{--    <div class="sub-menu collapse" id="settings">--}}
{{--        <a href=""><i class="fa fa-users p-1"></i>  Admins</a>--}}
{{--        <a href=""><i class="fa fa-id-badge p-1"></i> Roles</a>--}}
{{--        <a href=""><i class="fa fa-bus p-1"></i> Region</a>--}}
{{--        <a href=""><i class="fa fa-th-large p-1"></i> Loan plan</a>--}}
{{--        <a href=""><i class="fa fa-building-o p-1"></i> Business types</a>--}}
{{--    </div>--}}

    {{-- logout   --}}
    <a href="{{ url('/admin/logout') }}" class="{{ Request::path() == 'admin/logout' ? 'active-nav' : '' }}"
       onclick="event.preventDefault();
           document.getElementById('logout-form').submit();">
        <i class="fa fa-sign-out"></i> Logout
    </a>

    <form id="logout-form" action="{{ url('/admin/logout') }}" method="POST"
          style="display: none;">
        {{ csrf_field() }}
    </form>
</div>


