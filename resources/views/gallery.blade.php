@extends('layouts.master')
@section('content')
<div style="margin-top: 50px;"></div>
<div style="">
    <h1 class="" style="text-align: center; font-size: 40px;">My paintings</h1>
    <img style="margin-left: 38%; height: 30px;" src="{{asset('images/antique-line.png')}}">
{{--    <div class="flex-container">--}}
{{--        <img class="gallery" src="{{asset('images/gallery1.jpeg')}}">--}}
{{--        <img class="gallery" src="{{asset('images/gallery2.jpeg')}}">--}}
{{--        <img class="gallery" src="{{asset('images/gallery3.jpeg')}}">--}}
{{--        <img class="gallery" src="{{asset('images/gallery4.jpeg')}}">--}}
{{--    </div>--}}
{{--    <div class="flex-container">--}}
{{--        <img class="gallery" src="{{asset('images/gallery5.jpeg')}}">--}}
{{--        <img class="gallery" src="{{asset('images/gallery6.jpeg')}}">--}}
{{--        <img class="gallery" src="{{asset('images/gallery7.jpeg')}}">--}}
{{--        <div class="gallery8 gallery"> <div class="gallery-text">No need to lie</div></div>--}}
{{--    </div>--}}
    <div class="row">
        @foreach($paintings as $painting)
            <div class="col-md-4">
                <div class="thumbnail">
                    <img src="{{asset('storage/painting/'.$painting->image_url)}}" style="width:100%; height: 250px;">
                    <div class="caption">
{{--                        <p style="text-align: center;">--}}
{{--                            <a href="{{route('paintings.edit', $painting->id)}}"><i class="fa fa-pencil-square-o center" aria-hidden="true"></i></a>--}}
{{--                            <a href="{{route('paintings.destroy', $painting->id)}}"><i class="fa fa-trash-o " aria-hidden="true"></i></a>--}}
{{--                        </p>--}}
                    </div>
                    <div style=" margin-bottom: 5px">&nbsp;</div>
                </div>
            </div>
        @endforeach

    </div>
    {{--    <h1></h1>--}}
    <h1 class="" style="text-align: center; font-size: 40px;">My drawings</h1>
    <img style="margin-left: 38%; height: 30px;" src="{{asset('images/antique-line.png')}}">
    <div class="row">
        @foreach($drawings as $drawing)
            <div class="col-md-4">
                <div class="thumbnail">
                    <img src="{{asset('storage/drawing/'.$drawing->image_url)}}" style="width:100%; height: 250px;">
                    <div class="caption">
                        <a href="{{route('drawings.destroy', $drawing->id)}}"><i class="fa fa-trash-o pull-right" aria-hidden="true"></i></a>
                    </div>
                    <div style=" margin-bottom: 5px">&nbsp;</div>
                </div>
            </div>
        @endforeach

    </div>
</div>
@endsection
