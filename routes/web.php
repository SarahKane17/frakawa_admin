<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware'=>'admin_guest'],function () {
    Route::get('/', function () {
        return view('home');
    });
    Route::get('/gallery', 'GalleryController@index')->name('gallery');

//    Auth::routes();


    //Auth routes
    Route::get('/new/login', 'Admin\Auth\LoginController@showLoginForm')->name('admin.login');
    Route::post('/new/login','Admin\Auth\LoginController@Login')->name('admin.login');

});

//Route::get('/admin/login', 'Admin\Auth\LoginController@showLoginForm') -> name('admin.login');
//admin password reset
//Route::post('admin/password/email','Admin\Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
//Route::get('admin/password/reset','Admin\Auth\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
//Route::post('admin/password/reset','Admin\Auth\ResetPasswordController@reset');
//Route::get('admin/password/reset/{token}','Admin\Auth\ResetPasswordController@showResetForm')->name('admin.password.reset');
//



Route::group(['middleware'=>'admin_auth'],function () {
    //admin dashboard
    Route::get('/home', function (){
        return view('Admin/dashboard');
    });
    Route::post('/admin/logout', 'Admin\Auth\LoginController@logout')->name('admin.logout');

    //about me
    Route::prefix('about/me')->group(function () {
        Route::get('/', 'Admin\AboutMeController@index')->name('about_me');
        Route::get('/edit/{id}', 'Admin\AboutMeController@edit')->name('about_me.edit');
        Route::post('/update/{id}', 'Admin\AboutMeController@update')->name('about_me.update');
    });

    //paintings
    Route::prefix('paintings')->group(function () {
        Route::get('/', 'Admin\PaintingController@index')->name('paintings');
        Route::get('/new', 'Admin\PaintingController@new')->name('paintings.new');
        Route::post('/create', 'Admin\PaintingController@create')->name('paintings.create');
        Route::get('/edit/{id}', 'Admin\PaintingController@edit')->name('paintings.edit');
        Route::post('/update/{id}', 'Admin\PaintingController@update')->name('paintings.update');
        Route::get('/delete/{id}', 'Admin\PaintingController@destroy')->name('paintings.destroy');
    });

    //drawings
    Route::prefix('drawings')->group(function () {
        Route::get('/', 'Admin\DrawingController@index')->name('drawings');
        Route::get('/new', 'Admin\DrawingController@new')->name('drawings.new');
        Route::post('/create', 'Admin\DrawingController@create')->name('drawings.create');
        Route::get('/delete/{id}', 'Admin\DrawingController@destroy')->name('drawings.destroy');
    });

    //messages
    Route::prefix('messages')->group(function () {
        Route::get('/', 'Admin\MessageController@index')->name('messages');
        Route::post('/create', 'Admin\MessageController@create')->name('messages.create');
        Route::get('/delete/{id}', 'Admin\MessageController@destroy')->name('messages.destroy');
    });

});
