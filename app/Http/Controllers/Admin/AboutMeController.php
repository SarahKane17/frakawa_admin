<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Painting;
use App\User;
use Illuminate\Http\Request;

class AboutMeController extends Controller
{
    //
    public function index(){
        $data = [
            'users' => User::all(),
            'painting' => Painting::where('status', 2)->first(),
        ];
        return view('Admin.about_me.home', $data);
    }
    public function edit($id)
    {
        $data =[
          'user' => User::findorfail($id),
        ];
        return view('Admin.about_me.edit', $data);
    }
    public function update(Request $request, $id)
    {
        $user = User::findorfail($id);
        $user -> details = $request -> about_me;
        $user->save();
        return redirect('/about/me');
    }
}
