<?php

namespace App\Http\Controllers\Admin;

use App\Drawing;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DrawingController extends Controller
{
    //
    public function index()
    {
        $data = [
            'drawings' => Drawing::all(),
        ];
        return view('Admin.drawings.home', $data);
    }
    public function new()
    {
        return view('Admin.drawings.new');
    }
    public function create(Request $request)
    {
        //store image
        $drawing = new Drawing();


        if($request->hasFile('image_url')) {

            $filenameWithExt = $request->file('image_url')->getClientOriginalName();

            //Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

            // Get just ext
            $extension = $request->file('image_url')->getClientOriginalExtension();

            // Filename to store
            $fileNameToStore = $filename . '_' . time() . '.' . $extension;
            $destination = './storage/drawing';
            $drawing-> image_url = $fileNameToStore;
            // Upload Image
            $request->file('image_url')->move($destination,$fileNameToStore);
        }


//        $drawing->status = $request->status;
        $drawing->save();

        return redirect('/drawings');
    }
    public function destroy($id)
    {
        $drawing = Drawing::findorfail($id);
        $drawing->delete($id);
        return redirect('/drawings');
    }
}
