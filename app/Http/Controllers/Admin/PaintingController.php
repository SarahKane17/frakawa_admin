<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Painting;
use Illuminate\Http\Request;

class PaintingController extends Controller
{
    //
    public function index()
    {
        $data = [
            'paintings' => Painting::all(),
        ];
        return view('Admin.paintings.home', $data);
    }
    public function new()
    {
        return view('Admin.paintings.new');
    }
    public function create(Request $request)
    {
        //store image
        $painting = new Painting();


        if($request->hasFile('image_url')) {

            $filenameWithExt = $request->file('image_url')->getClientOriginalName();

            //Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

            // Get just ext
            $extension = $request->file('image_url')->getClientOriginalExtension();

            // Filename to store
            $fileNameToStore = $filename . '_' . time() . '.' . $extension;
            $destination = './storage/painting';
            $painting-> image_url = $fileNameToStore;
            // Upload Image
            $request->file('image_url')->move($destination,$fileNameToStore);
        }


        $painting->status = $request->status;
        $painting->save();

        return redirect('/paintings');
    }
    public function edit($id)
    {
        $data = [
          'painting' =>   Painting::findorfail($id),
        ];
        return view('Admin.paintings.edit', $data);
    }
    public function update(Request $request, $id)
    {
        $painting = Painting::findorfail($id);
        $painting -> status = $request->status;
        $painting->save();

        return redirect('/paintings');
    }
    public function destroy($id)
    {
        $painting = Painting::findorfail($id);
        $painting->delete($id);
        return redirect('/paintings');
    }
}
