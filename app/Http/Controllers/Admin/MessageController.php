<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Message;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    //
    public function index()
    {
        $data = [
            'messages' => Message::all(),
        ];
        return view('Admin.messages.home', $data);
    }
    public function create(Request $request)
    {
        $message = new Message();
        $message->message = $request->message;
        $message->email = $request->email;
        $message->save();

        return redirect('/');
    }
    public function destroy($id)
    {
        $message = Message::findorfail($id);
        $message->delete($id);
        return redirect('/messages');
    }
}
