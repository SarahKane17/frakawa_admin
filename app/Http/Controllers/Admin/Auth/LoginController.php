<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    //
    use AuthenticatesUsers;

    protected $redirectTo = '/home';

    public function showLoginForm(){
        return view('Admin.Auth.login');
    }

    public function guard()
    {
        return Auth::guard('admin_web');
    }

    public function credentials(Request $request)
    {
        return [

            'email'=>$request->email,
            'password'=>$request->password,
        ];
    }
}
