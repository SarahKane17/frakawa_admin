<?php

namespace App\Http\Controllers;

use App\Drawing;
use App\Painting;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    //
    public function index()
    {
        $data = [
          'paintings' => Painting::all(),
          'drawings' => Drawing::all(),
        ];
        return view('gallery', $data);
    }
}
